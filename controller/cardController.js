const cardController = {}
const card = require("../model/cardSchema")
const redis = require("redis");

let redisClient;

(async () => {
    redisClient = redis.createClient();
    redisClient.on("error", (error) => console.error(`Error : ${error}`));
    console.log("redis server connected")
    await redisClient.connect();
})();

cardController.createCard = async (req, res) => {
    try {
        if (req.body.projectName === "Development" || req.body.projectName === "Design" || req.body.projectName === "Genral" || req.body.projectName === "Marketing") {
            const data = new card(req.body)
            const newCard = await data.save()
            return res.status(200).send(newCard)
        } else {
            return res.status(200).send({ message: "projectName Department is not available" })
        }
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

cardController.listCard = async (req, res) => {
    try {
        isCached = false
        const species = process.env.secretCacheKey
        const cacheResults = await redisClient.get(species);
        results = await card.find({})
        if (cacheResults) {
            isCached = true;
            results = JSON.parse(cacheResults);
            console.log("getcache")
        } else {
            if (results.length === 0) {
                throw "API returned an empty array";
            }
            console.log("setcache")
            await redisClient.set(species, JSON.stringify(results), { EX: 30 });
        }
        res.send({
            fromCache: isCached,
            data: results,
        });
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}




module.exports = cardController

