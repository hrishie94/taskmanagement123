const userController = {}
const { userData, oldPAsswordData } = require("../model/userSchema")
const bcrypt = require("bcryptjs")
require("dotenv").config()
const jwt = require("jsonwebtoken")
const cron = require("node-cron");



userController.create = async (req, res) => {
    try {
        const data = req.body
        // console.log(req.body,"body")
        let password = req.body.password
        let existedNumber = await userData.find({ mobileNumber: req.body.mobileNumber })
        // console.log(existedNumber,"data")
        if (!existedNumber.length) {
            console.log("out")
            let encryptedPassword = await bcrypt.hash(data.password, 10);
            data.password = encryptedPassword
            data.confirmPassword = encryptedPassword
            console.log(data, "data")
            const token = jwt.sign(
                { user_id: userData._id },
                process.env.TOKEN_KEY);
            const userDatas = new userData(data)
            await userDatas.save()
            // console.log(userData)
            userDatas.token = token;
            const passData = new oldPAsswordData()
            passData.mobileNumber = userDatas.mobileNumber
            passData.oldPassword.push(password)
            // console.log(passData)
            await passData.save()
            return res.status(200).json({ userDatas })
        }
        else if (existedNumber[0].mobileNumber === data.mobileNumber) {
            console.log(existedNumber, "in")
            return res.status(403).send({ "message": "this number already existed, Please try with new one " })
        }
    }
    catch (error) {
        return res.status(400).json({ message: error.message });
    }
}

userController.login = async (req, res) => {
    try {
        // const data = req.body
        // console.log(req.body.password)
        const data = await userData.find({ mobileNumber: req.body.mobileNumber })
        // console.log(data,"data")
        let password = req.body.password
        let hash = data[0].password
        const token = jwt.sign(
            { user_id: userData._id },
            process.env.TOKEN_KEY);
        data[0].token = token
        if (data.length) {
            bcrypt.compare(password, hash, function (err, isMatch) {
                if (err) {
                    return err
                } else if (!isMatch) {
                    //   console.log("Password doesn't match!")
                    return res.status(200).send({ message: "password doesn't match" })
                } else {
                    //   console.log("Password matches!")
                    return res.status(200).send({ message: "user login successfully", data })

                }
            })
        }
        else {
            // console.log(mobileNumber,"in")
            return res.status(200).send({ message: "user does not exist" })
        }
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

let genrateOtp = () => {
    var val = Math.floor(1000 + Math.random() * 9000);
    console.log(val);
    return val
}

userController.forgetPassword = async (req, res) => {
    try {
        // let number = req.body.number
        let data = await userData.find({ mobileNumber: req.body.number })
        console.log(data.password, "data")
        if (data.length) {
            cron.schedule("*/2 * * * *", async function () {
                console.log("---------------------");
                console.log("running a task every 2 minutes");
                return await userData.updateOne({ mobileNumber: req.body.number }, { $set: { otp: process.env.otp } })

            })
            let otp = genrateOtp()
            const updateOtp = await userData.updateMany({ mobileNumber: req.body.number }, { $set: { otp } })
            return res.status(200).send({ otp, message: "OTP expires in two minutes" })
        } else {
            return res.status(200).send({ message: "mobileNumber does not exist" })
        }
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

userController.reset = async (req, res) => {
    try {
        // let data = req.body
        // console.log(req.body.number)
        const approved = await userData.find({ mobileNumber: req.body.number, otp: req.body.otp })
        encryptedPassword = await bcrypt.hash(req.body.password, 10);
        const oldPassword = await oldPAsswordData.find({ mobileNumber: req.body.number })
        //   console.log(oldPassword[0].oldpassword)
        let Lastpswd = []
        Lastpswd = oldPassword[0].oldPassword.slice(-2)
        // console.log(Lastpswd,"old")
        if (approved.length) {
            // console.log("in")
            if (Lastpswd[0] === req.body.password || Lastpswd[1] === req.body.password) {
                return res.status(200).send({ message: "please enter different passsword from last 2 passwords" })
            }
            else {
                let newPassword = await userData.updateOne({ mobileNumber: req.body.number }, { $set: { password: encryptedPassword, otp: process.env.otp } })
                let oldPassUpdate = await oldPAsswordData.updateOne({ mobileNumber: req.body.number }, { $push: { oldPassword: req.body.password } })
                return res.status(200).send({ message: "password has changed successfully" })
            }
        } else {
            return res.status(200).send({ message: "number or otp is not matching" })
        }
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}


module.exports = userController
