const mongoose = require("mongoose")
require("dotenv").config();
const URI = process.env.mongo_uri
mongoose.set('strictQuery', false);
const connectDB = async () => {
    await mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    console.log('the mongodb database has connected')
}
module.exports = connectDB;
