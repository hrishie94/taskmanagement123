const mongoose = require("mongoose")
const Schema = mongoose.Schema

const userSchema = new Schema({
  first_name: { type: String, default: require },
  last_name: { type: String, default: require },
  mobileNumber: { type: Number, default: require },
  password: { type: String, default: require },
  confirmPassword:{ type: String, default: require },
  token: { type: String },
  otp: { type: Number }
});

const oldpasswordSchema = new Schema({
  oldPassword: { type: Array },
  mobileNumber: { type: Number, default: require },
});

let userData = mongoose.model("user", userSchema)
let oldPAsswordData = mongoose.model("oldpassword", oldpasswordSchema)

module.exports = { userData, oldPAsswordData }