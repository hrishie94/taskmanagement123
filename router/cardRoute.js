const express = require("express")
const router = express.Router()
const cardController = require("../controller/cardController")
const auth = require("../middleware/auth")

router.post("/createCard", auth, cardController.createCard)

router.get("/listCard", auth, cardController.listCard)

module.exports = router
