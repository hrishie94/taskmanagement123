const express = require("express")
const router = express.Router()
const userController = require("../controller/userController")
const auth = require("../middleware/auth")
const { userDataValidate } = require("../middleware/validator")

router.post("/signup", userDataValidate, userController.create)

router.post("/login", userController.login)

router.post("/forgetpassword", auth, userController.forgetPassword)

router.post("/reset", auth, userController.reset)


module.exports = router